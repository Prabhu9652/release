'use strict';

const fs = require('fs');

let rawdata = fs.readFileSync('release.json');
let release = JSON.parse(rawdata);

release.hotFixRelease = Number(release.hotFixRelease) + 1;
fs.writeFileSync('release.json', JSON.stringify(release));
fs.writeFileSync('release.properties', 'ANUDAN_RELEASE_VERSION=' + release.releaseCandidate + '\nANUDAN_PRODUCTION_RELEASE_VERSION=' + release.productionRelease + '\nANUDAN_HOTFIX_RELEASE_VERSION=' + release.hotFixRelease);
