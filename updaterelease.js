'use strict';

const fs = require('fs');

let rawdata = fs.readFileSync('release.json');
let release = JSON.parse(rawdata);
let versionBreakup = release.releaseCandidate.split(".")
versionBreakup[2] = Number(versionBreakup[2]) + 1;
release.hotFixRelease = "0";
release.releaseCandidate = versionBreakup[0] + '.' + versionBreakup[1] + '.' + versionBreakup[2];
fs.writeFileSync('release.json', JSON.stringify(release));
fs.writeFileSync('release.properties', 'ANUDAN_RELEASE_VERSION=' + release.releaseCandidate + '\nANUDAN_PRODUCTION_RELEASE_VERSION=' + release.productionRelease + '\nANUDAN_HOTFIX_RELEASE_VERSION=' + release.hotFixRelease);

