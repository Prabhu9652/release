'use strict';

const fs = require('fs');

let rawdata = fs.readFileSync('release.json');
let release = JSON.parse(rawdata);
let versionBreakup = release.releaseCandidate.split(".")
versionBreakup[2] = Number(versionBreakup[2]) + 1;
console.log('Major: ' + versionBreakup[0] + '  Minor: ' + versionBreakup[1] + '   Build: ' + versionBreakup[2]);

release.productionRelease = release.releaseCandidate;
fs.writeFileSync('release.json', JSON.stringify(release));
fs.writeFileSync('release.properties', 'ANUDAN_RELEASE_VERSION=' + release.releaseCandidate + '\nANUDAN_PRODUCTION_RELEASE_VERSION=' + release.productionRelease + '\nANUDAN_HOTFIX_RELEASE_VERSION=' + release.hotFixRelease);
